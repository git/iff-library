(*---------------------------------------------------------------------------
    :Program.    IFFLib
    :Author.     Volker Rudolph/Thomas Wagner & Frédéric Pröls
    :Version.    1.2
    :Copyright.  PD
    :Language.   Oberon
    :Translator. Amiga-Oberon V2.14
    :Imports.    iff.library - Christian A. Weber
    :Contents.   Oberon library-module for iff.library
    :Remark.     Needs iff.library (Version 22 or higher) in libs: directory
    :Remark.     This ist the Oberon-version of the
    :Remark.     IFFLib-Module for M2Amiga by Fridtjof Siebert
    :History.    V1.0 by Volker Rudolph
    :History.    V1.1 by Thomas Wagner & Frédéric Pröls
    :History.         Update to iff.library version 22 (29-Jul-92)
    :History.    V1.2 by F. Siebert small chgs. for Amiga Oberon 3.0
    :History.    V1.3 by André Voget, changed all RECORDs to STRUCTs
    :bugs.       not tested
---------------------------------------------------------------------------*)

MODULE IFFLib;

IMPORT e := Exec,
            Graphics,
       i := Intuition,
       sys:=SYSTEM;

CONST
  iffName * = "iff.library";
  iffVersion * = 22;                  (* Current library version *)


(************** E R R O R - C O D E S ***********************************)

errorbadTask         * =     -1;      (* IffError() called by wrong task *)
erroropen            * =     16;      (* Can't open file *)
errorread            * =     17;      (* Error reading file *)
errornoMem           * =     18;      (* Not enough memory *)
errornotIff          * =     19;      (* File is not an IFF file *)
errorwrite           * =     20;      (* Error writing file *)
errornoILBM          * =     24;      (* IFF file is not of type ILBM *)
errornoBMHD          * =     25;      (* BMHD chunk not found *)
errornoBODY          * =     26;      (* BODY chunk not found *)
errorbadCompression  * =     28;      (* Unknown compression type *)
errornoANHD          * =     29;      (* ANHD chunk not found *)
errornoDLTA          * =     30;      (* DLTA chunk not found *)

(************** Common IFF IDs ******************************************)

(*
 * Universal IFF identifiers.
 *)

  idFORM  * = sys.VAL(LONGINT,"FORM");
  idLIST  * = sys.VAL(LONGINT,"LIST");
  idCAT   * = sys.VAL(LONGINT,"CAT ");
  idPROP  * = sys.VAL(LONGINT,"PROP");
  idNULL  * = sys.VAL(LONGINT,"    ");

(*
 *      Specific IFF IDs
*)

  idANIM  * = sys.VAL(LONGINT,"ANIM");
  idANHD  * = sys.VAL(LONGINT,"ANHD");
  idANNO  * = sys.VAL(LONGINT,"ANNO");
  idBMHD  * = sys.VAL(LONGINT,"BMHD");
  idBODY  * = sys.VAL(LONGINT,"BODY");
  idCAMG  * = sys.VAL(LONGINT,"CAMG");
  idCLUT  * = sys.VAL(LONGINT,"CLUT");
  idCMAP  * = sys.VAL(LONGINT,"CMAP");
  idCRNG  * = sys.VAL(LONGINT,"CRNG");
  idCTBL  * = sys.VAL(LONGINT,"CTBL");
  idDLTA  * = sys.VAL(LONGINT,"DLTA");
  idILBM  * = sys.VAL(LONGINT,"ILBM");
  idSHAM  * = sys.VAL(LONGINT,"SHAM");

  id8SVX  * = sys.VAL(LONGINT,"8SVX");
  idATAK  * = sys.VAL(LONGINT,"ATAK");
  idNAME  * = sys.VAL(LONGINT,"NAME");
  idRLSE  * = sys.VAL(LONGINT,"RLSE");
  idVHDR  * = sys.VAL(LONGINT,"VHDR");

(****************************************************************************
 *      Modes for OpenIFF()
 *)

  modeRead  * = 0;
  modeWrite * = 1;


(****************************************************************************
 *      Modes for CompressBlock() and DecompressBlock()
 *)

comprNone       * =  0000H;          (* generic *)
comprByteRun1   * =  0001H;          (* ILBM *)
comprFibDelta   * =  0101H;          (* 8SVX *)



(************** S T R U C T U R E S *************************************)

TYPE
  HandlePtr * = UNTRACED POINTER TO Handle;
  Handle    * = STRUCT            (* private ! *)
    file      : e.BPTR;       (* DOS file handle *)
    formSize  : LONGINT;      (* Größe des gesamten FORM *)
    chunkSize : LONGINT;      (* Größe des aktuellen Chunks *)
    chunkFPos : LONGINT;      (* Position im File wo dieser Chunk beginnt *)
  END;

  ChunkPtr * = UNTRACED POINTER TO Chunk;
  Chunk    * = STRUCT
    ckID   * : LONGINT;
    ckSize * : LONGINT;
    ckData * : ARRAY 1048576 OF SHORTINT; (* ARRAY ckSize OF SHORTINT *)
  END;

  BitMapHeaderPtr * = UNTRACED POINTER TO BitMapHeader;
  BitMapHeader    * = STRUCT
    w* , h*, x*, y   * : INTEGER;
    nPlanes          * : SHORTINT;
    masking          * : SHORTINT;
    compression      * : SHORTINT;
    pad1             * : SHORTINT;
    transparentColor * : INTEGER;
    xAspect          * : SHORTINT;
    yAspect          * : SHORTINT;
    pageWidth        * : INTEGER;
    pageHeight       * : INTEGER;
  END;

  AnimHeaderPtr * = UNTRACED POINTER TO AnimHeader;
  AnimHeader    * = STRUCT
    operation  * : SHORTINT;
    mask       * : SHORTINT;
    w*,h*,x*,y * : INTEGER;
    absTime    * : LONGINT;
    relTime    * : LONGINT;
    interLeave * : SHORTINT;
    pad0       * : SHORTINT;
    bits       * : LONGINT;
    pad        * : ARRAY 16 OF SHORTINT;
  END;

(*********************** L I B R A R Y - B A S E ************************)

VAR
  base * : e.LibraryPtr;

(************** F U N C T I O N   D E C L A R A T I O N S ***************)

(* obsolete *)
PROCEDURE OldOpenIFF * {base,-30}(fileName{8}: ARRAY OF CHAR): HandlePtr;

PROCEDURE CloseIFF * {base,-36}(iffFile{9}: HandlePtr);

PROCEDURE FindChunk * {base,-42}(iffFile{9}: HandlePtr;
                                 hunkName{0}: LONGINT): ChunkPtr;

PROCEDURE GetBMHD * {base,-48}(iffFile{9}: HandlePtr): BitMapHeaderPtr;

PROCEDURE GetColorMap * {base,-54}(iffFile{9}: HandlePtr;
                                   colorTable{8}: ARRAY OF INTEGER): LONGINT;

PROCEDURE DecodePic * {base,-60}(iffFile{9}: HandlePtr;
                                 bitmap{8}: Graphics.BitMapPtr): BOOLEAN;

PROCEDURE SaveBitMap * {base,-66}(fileName{8}: ARRAY OF CHAR;
                                  bitMap{9}: Graphics.BitMapPtr;
                                  colorTable{10}: ARRAY OF CHAR;
                                  flags{0}: SET): BOOLEAN;

PROCEDURE SaveClip * {base,-72}(fileName{8}: ARRAY OF CHAR;
                                bitmap{9}: Graphics.BitMapPtr;
                                coltab{10}: ARRAY OF CHAR;
                                flags{0}: SET;
                                xoff{1}, yoff{2}: INTEGER;
                                width{3}, height{4}: INTEGER): BOOLEAN;

PROCEDURE IffError * {base,-78}(): LONGINT;

PROCEDURE GetViewModes * {base,-84}(iffFile{9}: HandlePtr): LONGSET;

(* private and obsolete *)
PROCEDURE OldNewOpenIFF * {base,-90}(name{8}: ARRAY OF CHAR;
                                     memattr{0}: LONGSET): e.ADDRESS;

PROCEDURE ModifyFrame * {base,-96}(modifyform{9}: e.APTR;
                                   bitmap{8}: Graphics.BitMapPtr);

PROCEDURE OpenIFF * {base, -120}(name{8}: ARRAY OF CHAR;
                                 mode{0}: LONGINT): HandlePtr;

PROCEDURE PushChunk * {base, -126}(iff{8}: HandlePtr;
                                   type{0}: LONGINT;
                                   id{1}: LONGINT);

PROCEDURE PopChunk * {base, -132}(iff{8}: HandlePtr): BOOLEAN;

PROCEDURE WriteChunkBytes * {base, -138}(iff{8}: HandlePtr;
                                         buf{9}: ARRAY OF CHAR;
                                         size{0}: LONGINT);

PROCEDURE CompressBlock * {base, -144}(source{8}: ARRAY OF sys.BYTE;
                                       VAR destination{9}: ARRAY OF sys.BYTE;
                                       size{0}: LONGINT;
                                       mode{1}: LONGSET);

PROCEDURE DecompressBlock * {base, -150}(source{8}: ARRAY OF sys.BYTE;
                                         VAR destination{9}: ARRAY OF sys.BYTE;
                                         size{0}: LONGINT;
                                         mode{1}: LONGSET);



(* $OvflChk- $RangeChk- $StackChk- $NilChk- $ReturnChk- $CaseChk- *)
BEGIN

 base :=  e.OpenLibrary(iffName,iffVersion);

CLOSE

  IF base # NIL THEN
    e.CloseLibrary(base)
  END; (* IF *)

END IFFLib.

