##
##	$Id: Makefile,v 23.2 93/05/24 16:03:33 chris Exp $
##
##	Makefile to build and install iff.library, includes and fd files.
##
##	COPYRIGHT (C) 1987-1994 BY CHRISTIAN A. WEBER, BRUGGERWEG 2,
##	CH-8037 ZUERICH, SWITZERLAND.
##	THIS FILE MAY BE FREELY DISTRIBUTED. USE AT YOUR OWN RISK.
##


all:		iff.library Programmer/iff.lib


iff.library:
		@echo > RAM:make.bat "cd Source"
		@echo >>RAM:make.bat "make"
		@echo >>RAM:make.bat "cd /"
		@execute RAM:make.bat

Programmer/iff.lib:
		@echo > RAM:make.bat "cd Source/LinkLib"
		@echo >>RAM:make.bat "make"
		@echo >>RAM:make.bat "cd //"
		@execute RAM:make.bat

install:	iff.library Programmer/iff.lib
		Copy iff.library SYS:Libs-User/ CLONE
		Copy Programmer/iff.h INCUSR:libraries/ CLONE
		Copy Programmer/iff.i INCUSR:libraries/ CLONE
		Copy Programmer/iff.fd FD: CLONE
		Copy Programmer/iff.lib CCLIB: CLONE
		FlushLibs
		@FlushLibs

clean:
		@echo > RAM:make.bat "cd Source/LinkLib"
		@echo >>RAM:make.bat "make clean"
		@echo >>RAM:make.bat "cd /"
		@echo >>RAM:make.bat "make clean"
		@echo >>RAM:make.bat "cd /"
		@execute RAM:make.bat
		@delete iff.library Documentation/ifflib*.doc Documentation/ifflib*.guide

dist:
		execute arcbatch
