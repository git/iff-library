**
**	$Id: IFFLib.S,v 22.2 92/06/03 23:46:04 chris Exp $
**	$Revision: 22.2 $
**
**	$Filename: IFFLib.S $
**	$Author: chris $
**	$Date: 92/06/03 23:46:04 $
**
**	Disk-resident library for simple IFF file I/O. Does not handle
**	nested PROPs and CATs. Contains special ILBM support routines.
**
**	COPYRIGHT (C) 1987-1992 BY CHRISTIAN A. WEBER, BRUGGERWEG 2,
**	CH-8037 ZUERICH, SWITZERLAND.  ALL RIGHTS RESERVED.  NO PART
**	OF THIS SOFTWARE MAY BE COPIED, REPRODUCED,  OR  TRANSMITTED
**	IN ANY FORM OR BY ANY MEANS,  WITHOUT THE PRIOR WRITTEN PER-
**	MISSION OF THE AUTHOR. USE AT YOUR OWN RISK.
**
**	Modification history
**	--------------------
**
**	Date        Version  Modification
**
**	14-Sep-87   V 1.0   Created this file!
**	24-Oct-87   V 6.1   Writing crunched ILBMs now works
**	27-Nov-87   V 7.1   Fully multi-tasking compatible and reentrant
**	14-Dec-87   V 8.1   SaveClip() fixed
**	07-Feb-88   V 9.1   bmhd.Width is now WORD aligned, for DPaint
**	20-Feb-88   V 10.1  IffError() implemented, Source split into files
**	06-Mar-88   V 11.1  Stencil pictures are correctly decoded
**	08-Mar-88   V 12.1  GetViewModes() implemented
**	24-Mar-88   V 14.1  8SVX files go automatically to CHIP RAM
**	30-Jun-88   V 15.1  CMAP size bug fixed (3*64 instead of 2*32)
**	30-Jun-88   V 15.2  SaveClip() now generates CAMG chunks
**	20-Jul-88   V 15.3  Closes DOS library if started as a command
**	16-Nov-88   V 15.4  GetViewModes(): upto 384 pixels is now LoRes
**	30-Nov-88   V 15.5  DecodePic() rounds bmhd.width up to WORDs
**	01-Dec-88   V 16.1  NewOpenIFF() with MemType added
**	03-Mar-89   V 16.2  GetBMHD now sets IFFError (0 or IFF_NOBMHD)
**	18-Mar-89   V 16.3  GetViewModes(): upto 400 LoRes, 320 hor. NoLace
**	16-May-89   V 16.4  Code cleaned up, labels capitalized etc.
**	21-Feb-90   V 18.1  Uses CBM include files, GetColorTab() fixed
**	25-Feb-90   V 18.2  ModifyFrame() added
**	26 Feb-90   V 18.3  Decruncher handles corrupt pics & height=0
**	28-Feb-90   V 18.4  DecodePic() handles depth=1 again
**	12-Mar-90   V 18.5  OpenIFF() liest nicht mehr 4 Bytes zuviel ein
**	11-Apr-90   V 18.6  DecodePic geht auch wenn bm.Depth < bmh.Depth
**	13-Apr-90   V 18.7  DecodePic geht bis 24 Bitplanes
**	07-Oct-90   V 19.1  SaveClip() speichert 256 Farben bei 24 Planes
**	                    An neue 2.0 Includefiles angepasst (JSRLIB..)
**
**

		IDNT	IFFLib
		SECTION	text,CODE

* PP_VERSION
* DELEXP	; Wenn definiert wird Delayed Expunge unterstützt


		INCLUDE	"IFFLib.i"
		INCLUDE	"IFF_rev.i"

		XREF	FindOurNode		 ; Error-Node finden

		XREF	OldOpenIFFFunc
		XREF	CloseIFFFunc
		XREF	FindChunkFunc
		XREF	GetBMHDFunc
		XREF	GetColorTabFunc
		XREF	DecodePicFunc
		XREF	SaveBitMapFunc
		XREF	SaveClipFunc
		XREF	IFFErrorFunc
		XREF	GetViewModesFunc
		XREF	OldNewOpenIFFFunc
		XREF	ModifyFrameFunc
	IFD PP_VERSION
		XREF	PPOpenIFFFunc
	ENDC
		XREF	OpenIFFFunc
		XREF	PushChunkFunc
		XREF	PopChunkFunc
		XREF	WriteChunkBytesFunc
		XREF	CompressBlockFunc
		XREF	DecompressBlockFunc


	IFD PP_VERSION
VERSION:	EQU	101			 ; D0 bei OpenLibrary
REVISION:	EQU	4			 ; jedesmal um 1 erhöhen
	ENDC

*****************************************************************************
** Diese Routine wird aufgerufen wenn man iff.library als Programm startet

FirstByte:	lea	DOSName(PC),a1		; dos.library öffnen
		movea.l	4.W,a6
		JSRLIB	OldOpenLibrary
		movea.l	d0,a6			; DosBase
		JSRLIB	Output
		move.l	d0,d1			; file: stdout
		lea	MyLibInfo(PC),a0	; Adresse des ID-Strings
		move.l	a0,d2
		moveq.l	#MyLibInfoEnd-MyLibInfo,d3	; Länge des ID-Strings
		JSRLIB	Write
		movea.l	a6,a1
		movea.l	4.W,a6
		JSRLIB	CloseLibrary		; For Chris
		moveq.l	#10,d0			; return-code: ERROR
		rts

*****************************************************************************
**	So, nun das eigentliche Library-Zeug...

MyROMTag:	DC.W	RTC_MATCHWORD
		DC.L	MyROMTag	; RT_MATCHTAG
		DC.L	EndCode		; Ende der Library
		DC.B	RTF_AUTOINIT
		DC.B	VERSION
		DC.B	NT_LIBRARY
		DC.B	0		; Priorität
		DC.L	MyLibName	; Name der Bibliothek
		DC.L	MyLibInfo	; Name, Version, Revision etc.
		DC.L	Init		; Zeiger auf Init-Struktur

	IFD PP_VERSION
MyLibName:	DC.B	"iffpp.library",0
MyLibInfo:	DC.B	"IFFPP 101.4 (03.06.92) Copyright © 1990-1992 by Christian A. Weber",10,13,0
MyLibInfoEnd:
	ELSE
MyLibName:	DC.B	"iff.library",0
MyLibInfo:	VERS
		DC.B	" ("
		DATE
		DC.B	") (-: by Christian A. Weber :-)",13,10,0
MyLibInfoEnd:
	ENDC

DOSName:	DC.B	"dos.library",0

		EVEN
Init:		DC.L	iffb_SIZEOF	; data space size
		DC.L	FuncTable	; Zeiger auf Funktions-Initializers
		DC.L	DataTable	; Zeiger auf Data-Initializers
		DC.L	InitRoutine	; Initialisierungs-Routine


FuncTable: 	DC.W	-1		; Switch to WORD-Offset-Mode

	*** Die 4 Standard Library Routinen:

		DC.W	OpenFunc-FuncTable		; Offset -6
		DC.W	CloseFunc-FuncTable		; Offset -12
		DC.W	ExpungeFunc-FuncTable		; Offset -18
		DC.W	ExtFunc-FuncTable		; Offset -24

	*** Jetzt unsere IFF-Routinen:

		DC.W	OldOpenIFFFunc-FuncTable	; Offset -30
		DC.W	CloseIFFFunc-FuncTable		; Offset -36
		DC.W	FindChunkFunc-FuncTable		; Offset -42
		DC.W	GetBMHDFunc-FuncTable		; Offset -48
		DC.W	GetColorTabFunc-FuncTable	; Offset -54
		DC.W	DecodePicFunc-FuncTable		; Offset -60
		DC.W	SaveBitMapFunc-FuncTable	; Offset -66
		DC.W	SaveClipFunc-FuncTable		; Offset -72
		DC.W	IFFErrorFunc-FuncTable		; Offset -78
		DC.W	GetViewModesFunc-FuncTable	; Offset -84
		DC.W	OldNewOpenIFFFunc-FuncTable	; Offset -90
		DC.W	ModifyFrameFunc-FuncTable	; Offset -96
	IFD PP_VERSION
		DC.W	PPOpenIFFFunc-FuncTable		; Offset -102
	ELSE
		DC.W	ExtFunc-FuncTable		; Offset -102
	ENDC
		DC.W	ExtFunc-FuncTable		; Offset -108
		DC.W	ExtFunc-FuncTable		; Offset -114

		* V21-Routinen
		DC.W	OpenIFFFunc-FuncTable		; Offset -120
		DC.W	PushChunkFunc-FuncTable		; Offset -126
		DC.W	PopChunkFunc-FuncTable		; Offset -132
		DC.W	WriteChunkBytesFunc-FuncTable	; Offset -138
		DC.W	CompressBlockFunc-FuncTable	; Offset -144
		DC.W	DecompressBlockFunc-FuncTable	; Offset -150

		DC.W	-1				; End-Markierung

	*** Daten zur Initialisierung der Library-Struktur

DataTable:	INITBYTE  LN_TYPE,NT_LIBRARY
		INITLONG  LN_NAME,MyLibName
		INITBYTE  LIB_FLAGS,LIBF_SUMUSED|LIBF_CHANGED
		INITWORD  LIB_VERSION,VERSION
		INITWORD  LIB_REVISION,REVISION
		INITLONG  LIB_IDSTRING,MyLibInfo
		DC.L	  0

*****************************************************************************

	*** D0: Library Base, A0: Segment list, A6: SysBase

InitRoutine:	movem.l	d0-d1/a0-a1/a5,-(SP)
		movea.l	d0,a5			; Library pointer
		move.l	a6,iffb_SysBase(a5)	; Save SysBase
		move.l	a0,iffb_SegList(a5)	; Save SegList

	*** Dos-Library öffnen

		lea	DOSName(PC),a1		; dos.library öffnen
		JSRLIB	OldOpenLibrary
		move.l	d0,iffb_DOSBase(a5)	; DosBase merken

	*** Error-Liste initialisieren

		lea	iffb_ErrList(a5),a0
		NEWLIST	a0

		movem.l	(SP)+,d0-d1/a0-a1/a5	; d0/a5 wiederherstellen
		rts				; Library jetzt benutzbar

*****************************************************************************

	*** wird jedesmal bei OpenLibrary() aufgerufen, A6: IFFBase

OpenFunc:	movem.l	d1/a0-a2/a5-a6,-(SP)
		movea.l	a6,a5			; A5 :  IFFBase

	*** Testen ob schon ein Node für uns vorhanden ist

		bsr	FindOurNode		; Set Z bit if not found
		bne.b	.OpenError		; Schon vorhanden ---> Error

	*** Speicher für neuen Error-Node reservieren

		moveq.l	#ifferr_SIZEOF,d0		; Amount
		move.l	#MEMF_PUBLIC|MEMF_CLEAR,d1	; Requirements
		movea.l	iffb_SysBase(a5),a6
		JSRLIB	AllocMem
		tst.l	d0
		beq.b	.OpenError		; Error ---> 0 zurückgeben
		movea.l	d0,a2			; A2 :  ErrorNode

	*** Error-Node initialisieren: Task eintragen

		move.l	ThisTask(a6),ifferr_Task(a2)

	*** Error-Node an Liste anhängen

		lea	iffb_ErrList(a5),a0	; Liste
		movea.l	a2,a1			; Node
		JSRLIB	AddTail

	*** Normales Library-Zeug

		addq.w	#1,LIB_OPENCNT(a5)
		bclr.b	#LIBB_DELEXP,LIB_FLAGS(a5)
		move.l	a5,d0			; Return IFFBase
99$:
		movem.l	(SP)+,d1/a0-a2/a5-a6
		rts				; Library ist jetzt offen

.OpenError:	moveq.l	#0,d0			; ERROR!
		bra.b	99$

*****************************************************************************

	*** Wird jedesmal bei CloseLibrary() aufgerufen, A6: IFFBase

CloseFunc:	movem.l	a2/a5-a6,-(SP)
		movea.l	a6,a5			; IFFBase

	*** Error-Node suchen und entfernen

		bsr	FindOurNode		; our error-node nach D0 & A0
		beq.b	1$			; nicht gefunden --->
		movea.l	d0,a2			; ErrorNode retten
		movea.l	d0,a1
		movea.l	iffb_SysBase(a5),a6
		JSRLIB	Remove

		moveq.l	#ifferr_SIZEOF,d0
		movea.l	a2,a1
		JSRLIB	FreeMem
1$:		movem.l	(SP)+,a2/a5-a6

	*** Normales Library-Zeugs

		moveq.l	#0,d0			; return-code
		subq.w	#1,LIB_OPENCNT(a6)
	IFD DELEXP
		bne.b	2$			; jemand hat sie noch offen
		btst.b	#LIBB_DELEXP,LIB_FLAGS(a6) ; haben wir delayed Expunge?
		beq.b	2$			; neenee --->
		bsr.b	ExpungeFunc		; sonst Expunge
2$:
	ENDC
		rts				; Library ist jetzt zu

*****************************************************************************

	*** Wird beim letzten CloseLibrary aufgerufen, A6: IFFBase

ExpungeFunc:	movem.l	d2/a5-a6,-(SP)
		movea.l	a6,a5			; IFFBase

		tst.w	LIB_OPENCNT(a5)		; sind wir noch offen?
		beq.b	1$			; nein
	IFD DELEXP
		bset.b	#LIBB_DELEXP,LIB_FLAGS(a5)
	ENDC
		moveq.l	#0,d0			; return-code
		bra.b	99$
1$:
		movea.l	a5,a1			; LibPtr (zeigt auf NODE)
		movea.l	iffb_SysBase(a5),a6
		JSRLIB	Remove			; Library aus Liste entfernen

		movea.l	iffb_DOSBase(a5),a1	; dos.library schliessen
		JSRLIB	CloseLibrary
		move.l	iffb_SegList(a5),d2	; Segmentliste retten

		moveq.l	#0,d0
		movea.l	a5,a1			; LibPtr
		move.w	LIB_NEGSIZE(a5),d0	; Grösse der Sprungtabelle
		suba.w	d0,a1			; A1 := Anfang der Sprungtab.
		add.w	LIB_POSSIZE(a5),d0	; D0 := Lib-Grösse gesamt
		JSRLIB	FreeMem			; freigeben

		move.l	d2,d0			; return-code: Segmentliste

99$:		movem.l	(SP)+,d2/a5-a6
		rts

*****************************************************************************

	*** Wird aufgerufen bei Offset -24 und freien Slots

ExtFunc:	moveq.l	#0,d0
		rts

*****************************************************************************

EndCode:	; muss hier am Schluss stehen, siehe ROMTag-Struktur

		END
