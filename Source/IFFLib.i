**
**	$Id: IFFLib.i,v 22.1 92/06/02 16:48:13 chris Exp $
**	$Revision: 22.1 $
**
**	$Filename: Source/IFFLib.i $
**	$Author: chris $
**	$Date: 92/06/02 16:48:13 $
**
**	INTERNES Include-File für alle Module der IFF-Library
**
**	COPYRIGHT (C) 1987-1992 BY CHRISTIAN A. WEBER, BRUGGERWEG 2,
**	CH-8037 ZUERICH, SWITZERLAND.  ALL RIGHTS RESERVED.  NO PART
**	OF THIS SOFTWARE MAY BE COPIED, REPRODUCED,  OR  TRANSMITTED
**	IN ANY FORM OR BY ANY MEANS,  WITHOUT THE PRIOR WRITTEN PER-
**	MISSION OF THE AUTHOR. USE AT YOUR OWN RISK.
**

		OPT	O+,OW-

		INCLUDE	"exec/types.i"
		INCLUDE	"exec/memory.i"
		INCLUDE	"exec/nodes.i"
		INCLUDE	"exec/libraries.i"
		INCLUDE	"exec/resident.i"
		INCLUDE	"exec/initializers.i"
		INCLUDE "exec/macros.i"
		INCLUDE	"exec/execbase.i"
		INCLUDE	"dos/dos.i"
		INCLUDE	"graphics/gfx.i"

		INCDIR	INCUSR:
		INCLUDE	"libraries/iff.i"	; Standard IFF include file

*****************************************************************************

   STRUCTURE IFFBaseStructure,LIB_SIZE

	APTR	iffb_SysBase
	APTR	iffb_DOSBase
	APTR	iffb_SegList
	STRUCT	iffb_ErrList,LH_SIZE

	LABEL	iffb_SIZEOF

*****************************************************************************

   STRUCTURE IFFFileHandle,0

	ULONG	ifffh_Magic	; MUSS am Anfang sein
	BPTR	ifffh_File	; DOS Filehandle

	LABEL	iffh_CStack
	LONG	ifffh_ChunkSize	; Grösse des aktuellen Chunks/FORMs
	LONG	ifffh_ChunkFPos	; Position im File wo dieser Chunk beginnt

	STRUCT	morechunks,8*8	; insgesamt 8 Chunk nodes + 1 für Root-FORM
	LABEL	iffh_CStackEnd

	LABEL	ifffh_SIZEOF

IFFFH_MAGIC	EQU	$00496648	; 1. Byte MUSS 0 sein

*****************************************************************************

   STRUCTURE ErrorNode,LN_SIZE

	APTR	ifferr_Task
	LONG	ifferr_Error

	LABEL	ifferr_SIZEOF

