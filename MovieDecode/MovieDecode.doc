TABLE OF CONTENTS

MovieDecode/FreeJDecoder
MovieDecode/InitJDecoder
MovieDecode/ModifyJFrame
MovieDecode/FreeJDecoder                             MovieDecode/FreeJDecoder

    NAME
	FreeJDecoder -- Free the player code for an animation

    SYNOPSIS
	FreeJDecoder(handle)
	D0           A0

	VOID FreeJDecoder(APTR handle);

    FUNCTION
	Frees all resources which were allocated by InitJDecoder().
	This is typically called once after playing an animation.

    INPUTS
	hande - result from InitJDecoder() ONLY!

    RESULT
	none

    BUGS

    SEE ALSO
	InitJDecoder()

MovieDecode/InitJDecoder                             MovieDecode/InitJDecoder

    NAME
	InitJDecoder -- Initialize the player code for a given animation

    SYNOPSIS
	handle = InitJDecoder(animtype)
	D0                    D0

	APTR InitJDecoder(ULONG animtype);

    FUNCTION
	Initializes the movie player for a given animation type. This is
	typically called once after loading the animation. After playing,
	FreeJDecoder() should be called (with the handle this function
	returns) to free any resources allocated by this function.

    INPUTS
	animtype - Bits 0 to 7: Number of bitplanes (1..6)
	           Bit 16: mode 0 = "Move" (loop), 1 = "EOR" (ping-pong)

    RESULT
	Non-zero if everything was OK, NULL if no memory or bad animtype.

    BUGS

    SEE ALSO
	FreeJDecoder()

MovieDecode/ModifyJFrame                             MovieDecode/ModifyJFrame

    NAME
	ModifyJFrame -- Apply delta data on an animation frame

    SYNOPSIS
	success = ModifyJFrame( deltas, bitmap, handle )
	D0		        A0      A1      A2

	BOOL ModifyJFrame( APTR, struct BitMap *, APTR );

    FUNCTION
	Generates a new animation frame by changing the frame before the
	currently displayed frame (for double-buffering, as usual).

    INPUTS
	deltas - pointer to the actual DLTA chunk data
	bitmap - BitMap to render into
	handle - player handle, from InitJDecoder()

    RESULT
	Non-zero if everything was OK, FALSE if an error occurred.

    BUGS

    SEE ALSO
	InitJDecoder(), FreeJDecoder()

